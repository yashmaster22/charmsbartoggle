﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Windows.Forms;

namespace CharmsBarToggle
{
    /**
     * Finds the charms bar and clock handles.
     */
    class Charms
    {
        [DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]
        static extern IntPtr FindWindowByCaption(IntPtr ZeroOnly, string lpWindowName);

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, WindowShowStyle nCmdShow);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetWindowPlacement(IntPtr hWnd, ref WINDOWPLACEMENT lpwndpl);

        private struct WINDOWPLACEMENT
        {
            public int length;
            public int flags;
            public int showCmd;
            public System.Drawing.Point ptMinPosition;
            public System.Drawing.Point ptMaxPosition;
            public System.Drawing.Rectangle rcNormalPosition;
        }

        /**
         * Handle for the charms bar window
         */
        private IntPtr hWndCharmBar;

        /**
         * Handle for the clock window that appears with the charms bar
         */
        private IntPtr hWndCharmClock;

        public IntPtr GetCharmsBarHandle()
        {
            if (hWndCharmBar == null || hWndCharmBar == IntPtr.Zero)
            {
                hWndCharmBar = FindWindowByCaption(IntPtr.Zero, "Charm Bar");
            }

            return hWndCharmBar;
        }

        public IntPtr GetCharmsClockHandle()
        {
            if (hWndCharmClock == null || hWndCharmClock == IntPtr.Zero)
            {
                hWndCharmClock = FindWindowByCaption(IntPtr.Zero, "Clock and Date");
            }

            return hWndCharmClock;
        }

        public void HideCharmsBar() {
            if (GetCharmsBarHandle() != IntPtr.Zero)
            {
                Console.WriteLine("Hiding Charms Bar:{0}", GetCharmsBarHandle().ToString("X"));
                ShowWindow(GetCharmsBarHandle(), WindowShowStyle.ShowMinimized);
            }
        }

        public void ShowCharmsBar()
        {
            if (GetCharmsBarHandle() != IntPtr.Zero)
            {
                Console.WriteLine("Showing Charms Bar:{0}", GetCharmsBarHandle().ToString("X"));
                ShowWindow(GetCharmsBarHandle(), WindowShowStyle.ShowNormal);
            }
        }

        public void HideCharmsClock()
        {
            if (GetCharmsClockHandle() != IntPtr.Zero)
            {
                Console.WriteLine("Hiding Charms Clock:{0}", GetCharmsClockHandle().ToString("X"));
                ShowWindow(GetCharmsClockHandle(), WindowShowStyle.ShowMinimized);
            }
        }

        public void ShowCharmsClock()
        {
            if (GetCharmsClockHandle() != IntPtr.Zero)
            {
                Console.WriteLine("Showing Charms Clock:{0}", GetCharmsClockHandle().ToString("X"));
                ShowWindow(GetCharmsClockHandle(), WindowShowStyle.ShowNormal);
            }
        }

        public bool isCharmsBarHidden()
        {
            WINDOWPLACEMENT placement = new WINDOWPLACEMENT();
            GetWindowPlacement(GetCharmsBarHandle(), ref placement);
            return placement.showCmd != (int)WindowShowStyle.ShowNormal;
        }

        public bool isCharmsClockHidden()
        {
            WINDOWPLACEMENT placement = new WINDOWPLACEMENT();
            GetWindowPlacement(GetCharmsClockHandle(), ref placement);
            return placement.showCmd != (int)WindowShowStyle.ShowNormal;
        }
    }
}
