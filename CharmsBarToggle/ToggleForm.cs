﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Timers;

namespace CharmsBarToggle
{
    public partial class ToggleForm : Form
    {
        private Charms charms = new Charms();
        private bool ignoreEvent = false;
        private RegistryKey STARTUP_KEY = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
        private const String STARTUP_KEY_NAME = "CharmsBarToggle";
        public const String HIDE_BAR_ON_STARTUP_FLAG = "--hide-charms-bar";
        public const String HIDE_CLOCK_ON_STARTUP_FLAG = "--hide-charms-clock";
        public const String AUTO_MINIMIZE_AFTER_STARTUP_FLAG = "--auto-minimize-after-startup";

        private bool startupHideBar = false;
        private bool startupHideClock = false;
        private bool startupAutoMinimize = false;

        private System.Windows.Forms.Timer statusTimer;
        private ContextMenu notificationMenu;

        private MenuItem notifyMenuShowAll;
        private MenuItem notifyMenuHideAll;

        private MenuItem notifyMenuShowCharmsBar;
        private MenuItem notifyMenuHideCharmsBar;

        private MenuItem notifyMenuShowCharmsClock;
        private MenuItem notifyMenuHideCharmsClock;

        private MenuItem notifyMenuExit;

        public ToggleForm(bool startupHideBar, bool startupHideClock, bool startupAutoMinimize)
        {
            InitializeComponent();

            this.startupHideBar = startupHideBar;
            this.startupHideClock = startupHideClock;
            this.startupAutoMinimize = startupAutoMinimize;

            this.statusTimer = new System.Windows.Forms.Timer();
            this.statusTimer.Interval = 1000/60;
            this.statusTimer.Tick += new EventHandler(handleStatusTimer);

            this.notificationMenu = new ContextMenu();
            this.notifyMenuShowAll = this.notificationMenu.MenuItems.Add("Show All", this.handleNotifyMenuShowAll);
            this.notifyMenuHideAll = this.notificationMenu.MenuItems.Add("Hide All", this.handleNotifyMenuHideAll);
            this.notificationMenu.MenuItems.Add("-");
            this.notifyMenuShowCharmsBar = this.notificationMenu.MenuItems.Add("Show Charms Bar", this.handleNotifyMenuShowCharmsBar);
            this.notifyMenuHideCharmsBar = this.notificationMenu.MenuItems.Add("Hide Charms Bar", this.handleNotifyMenuHideCharmsBar);
            this.notificationMenu.MenuItems.Add("-");
            this.notifyMenuShowCharmsClock = this.notificationMenu.MenuItems.Add("Show Charms Clock", this.handleNotifyMenuShowCharmsClock);
            this.notifyMenuHideCharmsClock = this.notificationMenu.MenuItems.Add("Hide Charms Clock", this.handleNotifyMenuHideCharmsClock);
            this.notificationMenu.MenuItems.Add("-");
            this.notifyMenuExit = this.notificationMenu.MenuItems.Add("Exit", this.handleNotifyMenuExit);

            this.notifyIcon.ContextMenu = this.notificationMenu;
        }

        private void handleNotifyMenuExit(Object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void handleNotifyMenuHideCharmsClock(Object senter, EventArgs e)
        {
            this.hideCharmsClock.Checked = true;
        }

        private void handleNotifyMenuShowCharmsClock(Object senter, EventArgs e)
        {
            this.showCharmsClock.Checked = true;
        }

        private void handleNotifyMenuHideCharmsBar(Object senter, EventArgs e)
        {
            this.hideCharmsBar.Checked = true;
        }

        private void handleNotifyMenuShowCharmsBar(Object senter, EventArgs e)
        {
            this.showCharmsBar.Checked = true;
        }

        private void handleNotifyMenuHideAll(Object senter, EventArgs e)
        {
            this.hideAll.Checked = true;
        }

        private void handleNotifyMenuShowAll(Object senter, EventArgs e)
        {
            this.showAll.Checked = true;
        }

        // Handling when the timer event has completed and we are checking the current status of the charms bar vs what we want
        private void handleStatusTimer(object source, EventArgs e)
        {
            if (this.hideCharmsBar.Checked && !this.charms.isCharmsBarHidden())
            {
                this.charms.HideCharmsBar();
            }

            if (this.hideCharmsClock.Checked && !this.charms.isCharmsClockHidden())
            {
                this.charms.HideCharmsClock();
            }

            this.Invoke(new Action(() => this.checkStatus()));
        }

        private void ToggleForm_Load(object sender, EventArgs e)
        {
            this.checkStatus();
            this.applyStartup();
        }

        private void applyStartup()
        {
            if (this.startupHideBar)
            {
                this.hideCharmsBar.Checked = true;
            }

            if (this.startupHideClock)
            {
                this.hideCharmsClock.Checked = true;
            }

            if (this.startupAutoMinimize)
            {
                this.WindowState = FormWindowState.Minimized;
                this.Hide();
            }

            this.statusTimer.Start();
        }

        private void checkStatus()
        {
            this.ignoreEvent = true;
            this.showCharmsBar.Checked = !this.charms.isCharmsBarHidden();
            this.hideCharmsBar.Checked = !this.showCharmsBar.Checked;
            this.notifyMenuShowCharmsBar.Visible = !this.showCharmsBar.Checked;
            this.notifyMenuHideCharmsBar.Visible = !this.hideCharmsBar.Checked;

            this.showCharmsClock.Checked = !this.charms.isCharmsClockHidden();
            this.hideCharmsClock.Checked = !this.showCharmsClock.Checked;
            this.notifyMenuShowCharmsClock.Visible = !this.showCharmsClock.Checked;
            this.notifyMenuHideCharmsClock.Visible = !this.hideCharmsClock.Checked;

            if (this.showCharmsBar.Checked && this.showCharmsClock.Checked)
            {
                this.showAll.Checked = true;
                this.hideAll.Checked = false;
                this.notifyMenuShowAll.Visible = false;
                this.notifyMenuHideAll.Visible = true;
            }
            else if (!this.showCharmsBar.Checked && !this.showCharmsClock.Checked)
            {
                this.showAll.Checked = false;
                this.hideAll.Checked = true;
                this.notifyMenuShowAll.Visible = true;
                this.notifyMenuHideAll.Visible = false;
            }
            else
            {
                this.showAll.Checked = false;
                this.hideAll.Checked = false;
                this.notifyMenuShowAll.Visible = true;
                this.notifyMenuHideAll.Visible = true;
            }

            String startupValue = (String)this.STARTUP_KEY.GetValue(ToggleForm.STARTUP_KEY_NAME, null);
            if (startupValue != null)
            {
                this.runAtStartup.Checked = true;

                this.minimizeAtStartup.Checked = startupValue.IndexOf(ToggleForm.AUTO_MINIMIZE_AFTER_STARTUP_FLAG) != -1;
                this.minimizeAtStartup.Enabled = true;

                this.hideCharmsBarOnStartup.Checked = startupValue.IndexOf(ToggleForm.HIDE_BAR_ON_STARTUP_FLAG) != -1;
                this.hideCharmsBarOnStartup.Enabled = true;

                this.hideCharmsClockOnStartup.Checked = startupValue.IndexOf(ToggleForm.HIDE_CLOCK_ON_STARTUP_FLAG) != -1;
                this.hideCharmsClockOnStartup.Enabled = true;
            }
            else
            {
                this.runAtStartup.Checked = false;

                this.minimizeAtStartup.Checked = false;
                this.minimizeAtStartup.Enabled = false;

                this.hideCharmsBarOnStartup.Checked = false;
                this.hideCharmsBarOnStartup.Enabled = false;

                this.hideCharmsClockOnStartup.Checked = false;
                this.hideCharmsClockOnStartup.Enabled = false;
            }

            this.ignoreEvent = false;
        }

        private void showCharmsBar_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.ignoreEvent)
            {
                if (this.showCharmsBar.Checked)
                {
                    this.charms.ShowCharmsBar();
                    this.checkStatus();
                }
            }
        }

        private void hideCharmsBar_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.ignoreEvent)
            {
                if (this.hideCharmsBar.Checked)
                {
                    this.charms.HideCharmsBar();
                    this.checkStatus();
                }
            }
        }

        private void showCharmsClock_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.ignoreEvent)
            {
                if (this.showCharmsClock.Checked)
                {
                    this.charms.ShowCharmsClock();
                    this.checkStatus();
                }
            }
        }

        private void hideCharmsClock_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.ignoreEvent)
            {
                if (this.hideCharmsClock.Checked)
                {
                    this.charms.HideCharmsClock();
                    this.checkStatus();
                }
            }
        }



        private void showAll_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.ignoreEvent)
            {
                if (this.showAll.Checked)
                {
                    this.charms.ShowCharmsBar();
                    this.charms.ShowCharmsClock();
                    this.checkStatus();
                }
            }
        }

        private void hideAll_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.ignoreEvent)
            {
                if (this.hideAll.Checked)
                {
                    this.charms.HideCharmsBar();
                    this.charms.HideCharmsClock();
                    this.checkStatus();
                }
            }
        }

        private void hideCharmsBarOnStartup_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.ignoreEvent)
            {
                if (this.runAtStartup.Checked)
                {
                    this.setStartupKeyAndOptions();
                }

                this.checkStatus();
            }
        }

        private void hideCharmsClockOnStartup_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.ignoreEvent)
            {
                if (this.runAtStartup.Checked)
                {
                    this.setStartupKeyAndOptions();
                }

                this.checkStatus();
            }
        }

        private void setStartupKeyAndOptions()
        {
            String command = Application.ExecutablePath;

            if (this.hideCharmsBarOnStartup.Checked)
            {
                command += " " + ToggleForm.HIDE_BAR_ON_STARTUP_FLAG;
            }

            if (this.hideCharmsClockOnStartup.Checked)
            {
                command += " " + ToggleForm.HIDE_CLOCK_ON_STARTUP_FLAG;
            }

            if (this.minimizeAtStartup.Checked)
            {
                command += " " + ToggleForm.AUTO_MINIMIZE_AFTER_STARTUP_FLAG;
            }

            this.STARTUP_KEY.SetValue(ToggleForm.STARTUP_KEY_NAME, command);
        }

        private void runAtStartup_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.ignoreEvent)
            {
                if (this.runAtStartup.Checked)
                {
                    this.setStartupKeyAndOptions();
                }
                else
                {
                    this.STARTUP_KEY.DeleteValue(STARTUP_KEY_NAME);
                }
                this.checkStatus();
            }
        }

        private void minimizeAtStartup_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.ignoreEvent)
            {
                if (this.runAtStartup.Checked)
                {
                    this.setStartupKeyAndOptions();
                }

                this.checkStatus();
            }
        }

        private void ToggleForm_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                this.notifyIcon.BalloonTipTitle = "Charms Bar Toggle";
                this.notifyIcon.BalloonTipText = "Charms Bar Toggle is still running.";
                this.notifyIcon.Visible = true;
                this.notifyIcon.ShowBalloonTip(5000);
                this.ShowInTaskbar = false;
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                this.notifyIcon.Visible = false;
                this.ShowInTaskbar = true;
            }
        }

        private void notifyIcon_DoubleClick(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
        }
    }
}
