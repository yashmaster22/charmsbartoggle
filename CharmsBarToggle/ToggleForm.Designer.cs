﻿namespace CharmsBarToggle
{
    partial class ToggleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToggleForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.hideCharmsBar = new System.Windows.Forms.RadioButton();
            this.showCharmsBar = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.hideCharmsClock = new System.Windows.Forms.RadioButton();
            this.showCharmsClock = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.hideAll = new System.Windows.Forms.RadioButton();
            this.showAll = new System.Windows.Forms.RadioButton();
            this.hideCharmsBarOnStartup = new System.Windows.Forms.CheckBox();
            this.hideCharmsClockOnStartup = new System.Windows.Forms.CheckBox();
            this.runAtStartup = new System.Windows.Forms.CheckBox();
            this.minimizeAtStartup = new System.Windows.Forms.CheckBox();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.hideCharmsBar);
            this.groupBox1.Controls.Add(this.showCharmsBar);
            this.groupBox1.Location = new System.Drawing.Point(13, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(164, 45);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Charms Bar";
            // 
            // hideCharmsBar
            // 
            this.hideCharmsBar.AutoSize = true;
            this.hideCharmsBar.Location = new System.Drawing.Point(65, 22);
            this.hideCharmsBar.Name = "hideCharmsBar";
            this.hideCharmsBar.Size = new System.Drawing.Size(47, 17);
            this.hideCharmsBar.TabIndex = 1;
            this.hideCharmsBar.TabStop = true;
            this.hideCharmsBar.Text = "Hide";
            this.hideCharmsBar.UseVisualStyleBackColor = true;
            this.hideCharmsBar.CheckedChanged += new System.EventHandler(this.hideCharmsBar_CheckedChanged);
            // 
            // showCharmsBar
            // 
            this.showCharmsBar.AutoSize = true;
            this.showCharmsBar.Location = new System.Drawing.Point(7, 20);
            this.showCharmsBar.Name = "showCharmsBar";
            this.showCharmsBar.Size = new System.Drawing.Size(52, 17);
            this.showCharmsBar.TabIndex = 0;
            this.showCharmsBar.TabStop = true;
            this.showCharmsBar.Text = "Show";
            this.showCharmsBar.UseVisualStyleBackColor = true;
            this.showCharmsBar.CheckedChanged += new System.EventHandler(this.showCharmsBar_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.hideCharmsClock);
            this.groupBox2.Controls.Add(this.showCharmsClock);
            this.groupBox2.Location = new System.Drawing.Point(13, 64);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(164, 45);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Charms Clock and Date";
            // 
            // hideCharmsClock
            // 
            this.hideCharmsClock.AutoSize = true;
            this.hideCharmsClock.Location = new System.Drawing.Point(66, 20);
            this.hideCharmsClock.Name = "hideCharmsClock";
            this.hideCharmsClock.Size = new System.Drawing.Size(47, 17);
            this.hideCharmsClock.TabIndex = 1;
            this.hideCharmsClock.TabStop = true;
            this.hideCharmsClock.Text = "Hide";
            this.hideCharmsClock.UseVisualStyleBackColor = true;
            this.hideCharmsClock.CheckedChanged += new System.EventHandler(this.hideCharmsClock_CheckedChanged);
            // 
            // showCharmsClock
            // 
            this.showCharmsClock.AutoSize = true;
            this.showCharmsClock.Location = new System.Drawing.Point(7, 20);
            this.showCharmsClock.Name = "showCharmsClock";
            this.showCharmsClock.Size = new System.Drawing.Size(52, 17);
            this.showCharmsClock.TabIndex = 0;
            this.showCharmsClock.TabStop = true;
            this.showCharmsClock.Text = "Show";
            this.showCharmsClock.UseVisualStyleBackColor = true;
            this.showCharmsClock.CheckedChanged += new System.EventHandler(this.showCharmsClock_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.hideAll);
            this.groupBox3.Controls.Add(this.showAll);
            this.groupBox3.Location = new System.Drawing.Point(13, 116);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(164, 48);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "All";
            // 
            // hideAll
            // 
            this.hideAll.AutoSize = true;
            this.hideAll.Location = new System.Drawing.Point(66, 20);
            this.hideAll.Name = "hideAll";
            this.hideAll.Size = new System.Drawing.Size(47, 17);
            this.hideAll.TabIndex = 1;
            this.hideAll.TabStop = true;
            this.hideAll.Text = "Hide";
            this.hideAll.UseVisualStyleBackColor = true;
            this.hideAll.CheckedChanged += new System.EventHandler(this.hideAll_CheckedChanged);
            // 
            // showAll
            // 
            this.showAll.AutoSize = true;
            this.showAll.Location = new System.Drawing.Point(7, 20);
            this.showAll.Name = "showAll";
            this.showAll.Size = new System.Drawing.Size(52, 17);
            this.showAll.TabIndex = 0;
            this.showAll.TabStop = true;
            this.showAll.Text = "Show";
            this.showAll.UseVisualStyleBackColor = true;
            this.showAll.CheckedChanged += new System.EventHandler(this.showAll_CheckedChanged);
            // 
            // hideCharmsBarOnStartup
            // 
            this.hideCharmsBarOnStartup.AutoSize = true;
            this.hideCharmsBarOnStartup.Location = new System.Drawing.Point(13, 218);
            this.hideCharmsBarOnStartup.Name = "hideCharmsBarOnStartup";
            this.hideCharmsBarOnStartup.Size = new System.Drawing.Size(154, 17);
            this.hideCharmsBarOnStartup.TabIndex = 3;
            this.hideCharmsBarOnStartup.Text = "Hide Charms Bar on Statup";
            this.hideCharmsBarOnStartup.UseVisualStyleBackColor = true;
            this.hideCharmsBarOnStartup.CheckedChanged += new System.EventHandler(this.hideCharmsBarOnStartup_CheckedChanged);
            // 
            // hideCharmsClockOnStartup
            // 
            this.hideCharmsClockOnStartup.AutoSize = true;
            this.hideCharmsClockOnStartup.Location = new System.Drawing.Point(13, 241);
            this.hideCharmsClockOnStartup.Name = "hideCharmsClockOnStartup";
            this.hideCharmsClockOnStartup.Size = new System.Drawing.Size(165, 17);
            this.hideCharmsClockOnStartup.TabIndex = 4;
            this.hideCharmsClockOnStartup.Text = "Hide Charms Clock on Statup";
            this.hideCharmsClockOnStartup.UseVisualStyleBackColor = true;
            this.hideCharmsClockOnStartup.CheckedChanged += new System.EventHandler(this.hideCharmsClockOnStartup_CheckedChanged);
            // 
            // runAtStartup
            // 
            this.runAtStartup.AutoSize = true;
            this.runAtStartup.Location = new System.Drawing.Point(13, 172);
            this.runAtStartup.Name = "runAtStartup";
            this.runAtStartup.Size = new System.Drawing.Size(95, 17);
            this.runAtStartup.TabIndex = 5;
            this.runAtStartup.Text = "Run at Startup";
            this.runAtStartup.UseVisualStyleBackColor = true;
            this.runAtStartup.CheckedChanged += new System.EventHandler(this.runAtStartup_CheckedChanged);
            // 
            // minimizeAtStartup
            // 
            this.minimizeAtStartup.AutoSize = true;
            this.minimizeAtStartup.Location = new System.Drawing.Point(13, 195);
            this.minimizeAtStartup.Name = "minimizeAtStartup";
            this.minimizeAtStartup.Size = new System.Drawing.Size(115, 17);
            this.minimizeAtStartup.TabIndex = 6;
            this.minimizeAtStartup.Text = "Minimize at Startup";
            this.minimizeAtStartup.UseVisualStyleBackColor = true;
            this.minimizeAtStartup.CheckedChanged += new System.EventHandler(this.minimizeAtStartup_CheckedChanged);
            // 
            // notifyIcon
            // 
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "Charms Bar Toggle";
            this.notifyIcon.DoubleClick += new System.EventHandler(this.notifyIcon_DoubleClick);
            // 
            // ToggleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(190, 266);
            this.Controls.Add(this.minimizeAtStartup);
            this.Controls.Add(this.runAtStartup);
            this.Controls.Add(this.hideCharmsClockOnStartup);
            this.Controls.Add(this.hideCharmsBarOnStartup);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(206, 305);
            this.MinimumSize = new System.Drawing.Size(206, 305);
            this.Name = "ToggleForm";
            this.Text = "Charms Bar Toggle";
            this.Load += new System.EventHandler(this.ToggleForm_Load);
            this.Resize += new System.EventHandler(this.ToggleForm_Resize);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton hideCharmsBar;
        private System.Windows.Forms.RadioButton showCharmsBar;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton showCharmsClock;
        private System.Windows.Forms.RadioButton hideCharmsClock;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton hideAll;
        private System.Windows.Forms.RadioButton showAll;
        private System.Windows.Forms.CheckBox hideCharmsBarOnStartup;
        private System.Windows.Forms.CheckBox hideCharmsClockOnStartup;
        private System.Windows.Forms.CheckBox runAtStartup;
        private System.Windows.Forms.CheckBox minimizeAtStartup;
        private System.Windows.Forms.NotifyIcon notifyIcon;

    }
}

